﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public GameObject Player;
    public Text PlayerLifesText;
    public Text Score;
    public Text Enemies;
    
    // Start is called before the first frame update

    void Start()
    {
        //Player =GameObject.Find("Player");

    }

    // Update is called once per frame
    void Update()
    {
        PlayerLifesText.text = "Lifes" + GameManager.Instance.player.GetComponent<Variables>().lifes;
        Score.text = "Score " + GameManager.Instance.Score;
        Enemies.text = "Enemies " + GameManager.Instance.numEnemiesOnScreen;
       
    }
}
