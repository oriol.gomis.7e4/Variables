﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EnemyMoveCollider : MonoBehaviour
{
    public GameObject player;
    //public GameObject enemy;
    private float speed=0.5f;
    
    void Start()
    {
        
        player = GameManager.Instance.player;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);
    }
    
}
