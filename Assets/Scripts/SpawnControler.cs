﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnControler : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    private List<GameObject> enemies;
     

    public Vector2 randomX;
    public Vector2 randomY;
    // Start is called before the first frame update
    void Start()
    {
        //Crrida a la funcio amb el nom CreateEnemy amb un temporitzador de 1 fins a 5 segons 
        
        InvokeRepeating("CreateEnemy",1,1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemy() {
        //Lloc on es crea el monstre spawnejat

        //Pot serviv per guardar els enemics generats a un llista
        /*GameObject e= Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
        enemies.Add(e);*/

        Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
        GameManager.Instance.ContarEnemics(1);
    }
}
