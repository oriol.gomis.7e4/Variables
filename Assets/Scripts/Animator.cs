﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animator : MonoBehaviour
{
    
    public Sprite[] spr;
    public SpriteRenderer Sp;
    public Variables vr;
   
    public int contador = 0;
    public int seguiment = 0;

    public bool esgran = true;
    public int cont = 0;
    public int CDR = 0;
    public bool bot = false;

    public float deltatime=0.0f;
    //public Text text;
    float fps;
    int x = 4;
    int y = 4;

    public int i;
    public Button btn=null;

    void Start()
    {
        Application.targetFrameRate = 12;
        Sp = gameObject.GetComponent<SpriteRenderer>();
        vr = gameObject.GetComponent<Variables>();
        float vl= vr.speed / vr.weight;
        vr.speed = vl;
        //btn.onClick.AddListener(() => boto());
    }

    void Update()
    {
        deltatime += (Time.unscaledDeltaTime - deltatime) * 0.1f;
        fps = 1.0f / deltatime;

        //text.text = fps.ToString();
        /*accio();
        if (bot==true) {
            if (i <= vr.Height && esgran == true)
            {
                cont = 0;
                CDR = 0;
                giant();
                i++;

            }
            else if (i - 1 == vr.Height && cont <= 29)
            {
                cont++;
            }
            else if (i >= 0)
            {
                esgran = false;
                giantdown();
                i--;

            }
            else if (CDR  <= 30)
            {
                btn.enabled = false;
                CDR++;                      
            }
        }
        if (CDR == 30)
        {
            btn.enabled = true;
            esgran = true;
            bot = false;            
        }*/
        if (contador==3) {
            contador=0;
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.Translate(vr.speed * -Time.deltaTime, 0, 0);
            Sp.sprite = spr[contador];
            contador++;
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (contador==0) {
                transform.Rotate(0, 180, 0);
            }
     
           
            transform.Translate(0, 0, vr.speed * -Time.deltaTime);
            Sp.sprite = spr[contador];
            contador++;
        }
    }  
    
    public void accio() {
        if (contador < 3)
        {
            Sp.sprite = spr[contador];
            moviment();    
            contador++;
        }
        else
        {
            contador = 0;
        }
    }

    public void boto() {
        bot = true;  
    }

    public void moviment() {
        /* if (seguiment <= vr.distance)
         {
             transform.Translate(vr.speed * -Time.deltaTime, 0, 0);
             seguiment=seguiment+(int)(vr.speed);  
         }
         else
         {
             transform.Rotate(0, 180, 0);
             seguiment = 0;
         };*/

        
    }
    
    public void giant() {
        Sp.transform.localScale = new Vector3(x, y, 0);
        x=x+2;
        y=y+2;
    }

    public void giantdown()
    {
        Sp.transform.localScale = new Vector3(x, y, 0);
        x = y - 2;
        y = y - 2;
    }
}
    