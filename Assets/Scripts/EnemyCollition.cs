﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyCollition : MonoBehaviour
{
    [SerializeField]
    private int scorePoint = 5;
    
    private void OnTriggerEnter2D(Collider2D collision) {
        
        Debug.Log("has xocat");
        
        if (collision.CompareTag("Player")) 
        {
           
            //code to collition with player
            Debug.Log("colisio amb player");
            GameObject pl = collision.gameObject;
            pl.GetComponent<Variables>().lifes--;

            if (pl.GetComponent<Variables>().lifes <= 0)
            {

                Destroy(pl);
                SceneManager.LoadScene("Matar enemcs");
                
            }
            
            
        }
        Destroy(gameObject);
        GameManager.Instance.RestarEnemics(1);

    }

    private void OnMouseDown() {
        Destroy(gameObject);
        GameManager.Instance.RestarEnemics(1);
        GameManager.Instance.IncreasesScore(scorePoint);
    }
    
    
}
   


