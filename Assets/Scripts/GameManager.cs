﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameManager _instance;


    private int _score;
    public int numEnemiesOnScreen=1;
    public GameObject player;
    public UIController UIController;
   

    public static GameManager Instance {
        get
        {
            return _instance;
        }
    }
    private void Awake() {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    public int Score {
        get {
            return this._score;
        }
    }
    public int EnemiesScreen
    {
        get
        {
            return this.numEnemiesOnScreen;
        }
    }





    void Start()
    {
        _score = 0;
        numEnemiesOnScreen = 0;
        player = GameObject.Find("Player");
        UIController = GameObject.Find("Canvas").GetComponent<UIController>();

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ContarEnemics(int enemic) {
        this.numEnemiesOnScreen += enemic;
    }
    public void RestarEnemics(int enemic)
    {
        this.numEnemiesOnScreen -= enemic;
    }

    public void IncreasesScore(int increment) {
        this._score += increment;
    }

    
 
}
